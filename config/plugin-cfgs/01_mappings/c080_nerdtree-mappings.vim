let s:present = isdirectory(expand("$NVIM_PKGS_MINPAC") . "/start/nerdtree")
if(s:present)
    nnoremap <silent> <F3> :NERDTreeOrPrevBuffer<CR>
    inoremap <silent> <F3> <C-o>:NERDTreeOrPrevBuffer<CR>

    nnoremap <silent> <S-F3> :NERDTreeFind<CR>
    inoremap <silent> <S-F3> <C-o>:NERDTreeFind<CR>
    nnoremap <silent> <F15> :NERDTreeFind<CR>
    inoremap <silent> <F15> <C-o>:NERDTreeFind<CR>

    nnoremap <silent> <F1> :NERDTreeClose<CR>
    inoremap <silent> <F1> <C-o>:NERDTreeClose<CR>
endif

