let s:present = isdirectory(expand("$NVIM_PKGS_MINPAC") . "/start/vim-easy-align")
if(s:present)
    vmap <Enter> <Plug>(EasyAlign)
    nmap ga <Plug>(EasyAlign)
endif

