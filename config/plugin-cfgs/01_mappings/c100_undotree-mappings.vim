let s:present = isdirectory(expand("$NVIM_PKGS_MINPAC") . "/start/undotree")
if(s:present)
    nnoremap <silent> <Leader>u   :UndotreeToggle<CR>
    nnoremap <silent> <Leader>ju  :UndotreeShow<CR>
    nnoremap <silent> <Leader>U   :UndotreeHide<CR>
endif

