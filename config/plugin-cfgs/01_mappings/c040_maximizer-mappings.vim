let s:present = isdirectory(expand("$NVIM_PKGS_MINPAC") . "/start/vim-maximizer")
if(s:present)
    nnoremap <silent> <S-F1>  :MaximizerToggle!<CR>
    vnoremap <silent> <S-F1>  :MaximizerToggle!<CR>gv
    inoremap <silent> <S-F1>  <C-o>:MaximizerToggle!<CR>

    nnoremap <silent> <F13>  :MaximizerToggle!<CR>
    vnoremap <silent> <F13>  :MaximizerToggle!<CR>gv
    inoremap <silent> <F13>  <C-o>:MaximizerToggle!<CR>
endif

