let s:present = isdirectory(expand("$NVIM_PKGS_MINPAC") . "/start/undotree")
if(s:present)
    let g:undotree_WindowLayout = 3
    let g:undotree_SplitWidth = 38
endif

