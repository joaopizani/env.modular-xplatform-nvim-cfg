let s:present = isdirectory(expand("$NVIM_PKGS_MINPAC") . "/start/vim-sleuth")
if(s:present)
    let b:sleuth_automatic = 1
endif

