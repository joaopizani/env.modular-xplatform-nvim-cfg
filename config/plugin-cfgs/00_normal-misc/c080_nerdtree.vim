let s:present = isdirectory(expand("$NVIM_PKGS_MINPAC") . "/start/nerdtree")
if(s:present)

    let NERDTreeChDirMode = 2
    let NERDTreeWinSize = 28
    let NERDTreeMouseMode = 2
    let NERDTreeShowBookmarks = 1
    let NERDTreeMarkBookmarks = 1
    let NERDTreeMinimalMenu = 1
    let NERDTreeBookmarksFile = expand("$NVIM_RUNTIME") . '/NERDTreeBookmarks'

    function! s:NERDTreeOrPrevBuffer()
        let l:cur_filetype = &filetype
        if l:cur_filetype == "nerdtree"  " go to previous window
            :normal p
        else
            :NERDTreeFocus
        endif
    endfunction
    command! NERDTreeOrPrevBuffer call <SID>NERDTreeOrPrevBuffer()

endif

