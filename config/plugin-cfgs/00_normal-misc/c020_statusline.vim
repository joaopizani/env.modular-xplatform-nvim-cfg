let s:present = isdirectory(expand("$NVIM_PKGS_MINPAC") . "/start/vim-flagship")
if(s:present)

    set laststatus=2
    set showtabline=1
    set guioptions-=e

    autocmd User Flags call Hoist("window", "%{&ignorecase ? '[IC]' : ''}")
    autocmd User Flags call Hoist("window", "%{&smartcase ? '[SC]' : ''}")

endif

