let s:present = isdirectory(expand("$NVIM_PKGS_MINPAC") . "/start/vim-maximizer")

if(s:present)
    let g:maximizer_set_default_mapping = 0
endif

