let s:present = isdirectory(expand("$NVIM_PKGS_MINPAC") . "/start/awesome-vim-colorschemes")
if(s:present)
    if has('gui_running')
        set background=light
    endif
    colorscheme solarized8
endif

