function! s:WrapSoft()
    set textwidth=0
    set formatoptions=""
endfunction
command! SoftWrap call <SID>WrapSoft()

function! s:WrapHard()
    set textwidth=115
    set formatoptions=tanw
endfunction
command! HardWrap call <SID>WrapHard()


call <SID>WrapSoft()
set wrap linebreak
set showbreak=⋯  " unicode ellipsis to show that a line is wrapped
set colorcolumn=115

if exists('+breakindent')
    set breakindent
endif

