set shortmess+=filmnrxoOtT  " abbrev. of messages (avoids 'hit enter')
set viewoptions=folds,cursor,unix,slash  " better unix / windows compatibility
set history=500  " Store a ton of history (default is 20)
set cursorline  " highlight current line
set laststatus=2  " always display the status line

set autoindent  " for when syntax-specific indentation is off

set noequalalways  " otherwise sizes of existing windows wont be respected

set nohlsearch

set backspace=indent,eol,start

command! -nargs=1 Silent
\ | execute ':silent '.<q-args>
\ | execute ':redraw!'
