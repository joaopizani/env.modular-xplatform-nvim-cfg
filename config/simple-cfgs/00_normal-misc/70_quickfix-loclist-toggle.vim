" Functions for navigating back and forth between qf/ll window and previous

function! s:QFOrPrevBuffer()
    let l:cur_filetype = &filetype
    if l:cur_filetype == "qf"  " go to previous window
        :normal p
    else
        :copen
    endif
endfunction
command! QFOrPrevBuffer call <SID>QFOrPrevBuffer()

function! s:LLOrPrevBuffer()
    let l:cur_filetype = &filetype
    if l:cur_filetype == "qf"  " TODO: find way to distinguish from qf window
        :normal p
    else
        :lopen
    endif
endfunction
command! LLOrPrevBuffer call <SID>LLOrPrevBuffer()

