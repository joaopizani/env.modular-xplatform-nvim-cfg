set wildmenu
set wildmode=list:longest,full  " command <Tab> completion, list matches, then longest common.

set scrolljump=3                " lines to scroll when cursor leaves screen

set foldmethod=syntax
set foldlevelstart=3

" Highlight problematic whitespace
set listchars=tab:↹.,trail:⍽,extends:#,nbsp:⍽
set list

" Wrapped lines treatment
nnoremap j gj
nnoremap k gk

set ignorecase
set smartcase
