
" backups, persistent undo and view files
set backup  " backups are nice
if has('persistent_undo')
    set undofile  " so is persistent undo
    set undolevels=100  " maximum number of changes that can be undone
    set undoreload=3000  " maximum number lines to save for undo on a buffer reload
endif

set backupdir-=.  " take the CWD out of the list of backup dirs, leave only default ($XDG_STATE_HOME/nvim/backup)

" Could use * rather than *.*, but I prefer to leave .files unsaved
au BufWinLeave *.* silent! mkview    " make vim save view (state) (folds, cursor, etc)
au BufWinEnter *.* silent! loadview  " make vim load view (state) (folds, cursor, etc)

