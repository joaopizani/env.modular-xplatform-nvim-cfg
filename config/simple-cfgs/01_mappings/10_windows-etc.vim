" Easier moving in windows and resizing windows
noremap <C-j> <C-W>j
noremap <C-k> <C-W>k
noremap <C-l> <C-W>l
noremap <C-h> <C-W>h
noremap <C-p> <C-W>p
noremap <C-s> <C-W>s
nnoremap <A-Up>    <C-w>+
nnoremap <A-Down>  <C-w>-
nnoremap <A-Left>  <C-w><
nnoremap <A-Right> <C-w>>

" shortcut for activating U completion
imap <C-Space> <C-x><C-u>

