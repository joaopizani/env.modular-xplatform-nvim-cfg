nnoremap <silent> <F2> :w<CR>
inoremap <silent> <F2> <C-o>:w<CR>

nnoremap <silent> <S-F2> :wa<CR>
inoremap <silent> <S-F2> <C-o>:wa<CR>
nnoremap <silent> <F14> :wa<CR>
inoremap <silent> <F14> <C-o>:wa<CR>


nnoremap <silent> <F4> :silent! QFOrPrevBuffer<CR>
inoremap <silent> <F4> <C-o>:silent! QFOrPrevBuffer<CR>

nnoremap <silent> <S-F4> :silent! LLOrPrevBuffer<CR>
inoremap <silent> <S-F4> <C-o>:silent! LLOrPrevBuffer<CR>
nnoremap <silent> <F16> :silent! LLOrPrevBuffer<CR>
inoremap <silent> <F16> <C-o>:silent! LLOrPrevBuffer<CR>


nnoremap <silent> <F5> :crewind<CR>
inoremap <silent> <F5> <C-o>:crewind<CR>

nnoremap <silent> <S-F5> :lrewind<CR>
inoremap <silent> <S-F5> <C-o>:lrewind<CR>
nnoremap <silent> <F17> :lrewind<CR>
inoremap <silent> <F17> <C-o>:lrewind<CR>


nnoremap <silent> <F6> :cprevious<CR>
inoremap <silent> <F6> <C-o>:cprevious<CR>

nnoremap <silent> <S-F6> :lprevious<CR>
inoremap <silent> <S-F6> <C-o>:lprevious<CR>
nnoremap <silent> <F18> :lprevious<CR>
inoremap <silent> <F18> <C-o>:lprevious<CR>


nnoremap <silent> <F7> :cnext<CR>
inoremap <silent> <F7> <C-o>:cnext<CR>

nnoremap <silent> <S-F7> :lnext<CR>
inoremap <silent> <S-F7> <C-o>:lnext<CR>
nnoremap <silent> <F19> :lnext<CR>
inoremap <silent> <F19> <C-o>:lnext<CR>


nnoremap <silent> <F8> :cclose<CR>
inoremap <silent> <F8> <C-o>:cclose<CR>

nnoremap <silent> <S-F8> :lclose<CR>
inoremap <silent> <S-F8> <C-o>:lclose<CR>
nnoremap <silent> <F20> :lclose<CR>
inoremap <silent> <F20> <C-o>:lclose<CR>

