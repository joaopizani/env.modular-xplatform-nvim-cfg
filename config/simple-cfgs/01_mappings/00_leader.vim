nnoremap <space> <Nop>
let mapleader = " "
let maplocalleader = "\\"

" remove trailing whitespace from file
nnoremap <silent> <Leader>t
    \ :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>

nnoremap <silent> <Leader>s  :set hlsearch<CR>
nnoremap <silent> <Leader>S  :set nohlsearch<CR>

nnoremap <silent> <Leader>p  :set paste<CR>
nnoremap <silent> <Leader>P  :set nopaste<CR>

nnoremap <silent> <Leader>n  :set number<CR>
nnoremap <silent> <Leader>N  :set nonumber<CR>

nnoremap <silent> <Leader>r  :set relativenumber<CR>
nnoremap <silent> <Leader>R  :set norelativenumber<CR>

nnoremap <silent> <Leader>h :HardWrap<CR>
nnoremap <silent> <Leader>H  :SoftWrap<CR>

nnoremap <silent> <Leader>mm :Silent make!<CR>
nnoremap <silent> <Leader>mc :Silent make! clean<CR>
nnoremap <silent> <Leader>mC :Silent make! veryclean<CR>

