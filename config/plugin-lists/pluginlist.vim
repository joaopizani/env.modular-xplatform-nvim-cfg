" colorschemes
call minpac#add('rafi/awesome-vim-colorschemes')


" general-purpose plugins
call minpac#add('szw/vim-maximizer')

call minpac#add('scrooloose/nerdtree')
call minpac#add('mbbill/undotree')

call minpac#add('tomtom/tcomment_vim')
call minpac#add('junegunn/vim-easy-align')

call minpac#add('tpope/vim-flagship')

call minpac#add('tpope/vim-fugitive')

call minpac#add('yssl/QFEnter')


" indent stuff

" motions
call minpac#add('jeetsukumaran/vim-indentwise')

" objects
call minpac#add('michaeljsmith/vim-indent-object')

" heuristic settings based on current file or files of same type
call minpac#add('tpope/vim-sleuth')


" editorconfig
call minpac#add('editorconfig/editorconfig-vim')
