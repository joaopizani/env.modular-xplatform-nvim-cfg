let $NVIM_CONFIG = expand("$NVIM_ROOT") . '/config'
let $NVIM_RUNTIME = expand("$NVIM_ROOT") . '/runtime'
let $NVIM_CACHES = expand("$NVIM_RUNTIME") . '/caches'

if( ! exists($XDG_DATA_HOME) ) | let $XDG_DATA_HOME = expand("$HOME") . '/.local/share' | endif
let $NVIM_PKGS_ROOT = expand("$XDG_DATA_HOME") . '/nvim/site'
let $NVIM_PKGS_MINPAC = expand("$NVIM_PKGS_ROOT") . '/pack/minpac'

let $NVIM_EXT = expand("$HOME") . '/.nvim-cfg-extra'
let $NVIM_EXT_PLUGINLISTDIR = expand("$NVIM_EXT") . '/00_plugin-lists'
let $NVIM_EXT_INIT_BASIC = expand("$NVIM_EXT") . '/init-basic.vim'
let $NVIM_EXT_INIT_PLUGINS = expand("$NVIM_EXT") . '/init-plugins.vim'


" simple part - can be used with no extra plugins installed
for f in glob(expand("$NVIM_CONFIG") . '/simple-cfgs/**/*.vim', 1, 1)   | exe 'source' f | endfor

" plugin-dependent configs. Each of the included files should check if the plugin actually is installed
for f in glob(expand("$NVIM_CONFIG") . '/plugin-cfgs/**/*.vim', 1, 1) | exe 'source' f | endfor


" load the extra configs (out-of-repository) if present (basic)
if( filereadable(expand("$NVIM_EXT_INIT_BASIC")) ) | exe 'source' expand("$NVIM_EXT_INIT_BASIC") | endif

" load the extra configs (out-of-repository) if present (plugins)
if( filereadable(expand("$NVIM_EXT_INIT_PLUGINS")) ) | exe 'source' expand("$NVIM_EXT_INIT_PLUGINS") | endif


function! PackInit() abort
    " minpac must have {'type': 'opt'} so that it can be loaded with `packadd`.
    packadd minpac
    call minpac#init({ 'dir' : expand("$NVIM_PKGS_ROOT") })
    call minpac#add('k-takata/minpac', {'type': 'opt'})

    for f in glob(expand("$NVIM_CONFIG") . '/plugin-lists/**/*.vim', 1, 1) | exe 'source' f | endfor

    if( isdirectory(expand("$NVIM_EXT_PLUGINLISTDIR")) )
        for f in glob(expand("$NVIM_EXT_PLUGINLISTDIR") . '/**/*.vim', 1, 1) | exe 'source' f | endfor
    endif
endfunction

" Commands for managing plugins. Each calls PackInit() to load minpac and register plugins, then does the task.
command! PackUpdate call PackInit() | call minpac#update()
command! PackClean  call PackInit() | call minpac#clean()
command! PackStatus call PackInit() | call minpac#status()
