#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"


echo "Please make sure neovim is installed using whatever means necessary"


NVIMCFGDIR="${HOME}/.config/nvim"
mkdir -p "${NVIMCFGDIR}"
ln -s -f "${DIR}/_nvimrc"  "${NVIMCFGDIR}/init.vim"


CFG_HOME="${XDG_CONFIG_HOME:-"${HOME}/.config"}"
MINPAC_TRGDIR="${CFG_HOME}/nvim/pack/minpac/opt/minpac"
mkdir -p "${MINPAC_TRGDIR}"
git clone "https://github.com/k-takata/minpac.git" "${MINPAC_TRGDIR}"

nvim +PackInstall +qall


BASH_ALIASES_FILE="${HOME}/.bash_aliases"
BASH_ALIASES_DIR="${BASH_ALIASES_FILE}.d"

if [[ ! -d "${BASH_ALIASES_DIR}" ]]; then

mkdir -p "${BASH_ALIASES_DIR}"
cat >>"${BASH_ALIASES_FILE}" <<EOF

CONFDIR="${BASH_ALIASES_DIR}"
if [[ -d "\${CONFDIR}" ]]; then
  while IFS= read -r -d '' FNAME; do
    . "\${FNAME}"   # Process substitution instead of pipeline to avoid running in subshell
  done  <  <(find "\${CONFDIR}" -mindepth 1 -maxdepth 1 -name '*.sh' -print0)
fi

EOF

fi

ln -sfn "${DIR}/70-nvim-aliases.sh" "${BASH_ALIASES_DIR}"/

